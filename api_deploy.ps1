#az login
#az account set --subscription "Microsoft Azure Enterprise"    

$instanceId = $args[0];
$resourceGroup = $args[1];
$accessKey = $args[2];
$accessLogin = $args[3];

echo "#####################################"
echo "##       Digital Pages - v1.0      ##"
echo "#####################################"
echo ""
echo "Configurações:"
echo "Instance Id: $instanceId";
echo "Resouce Group: $resourceGroup";
echo ""
echo "Recuperando builds..."

$gitUrl = "https://"+$accessLogin+":"+$accessKey+"@bitbucket.org/digitalpages/csharp_services_builds.git";
$folder = $pwd.Path + "/csharp_services_builds";

if (Test-Path -Path $folder -PathType Container)
{
    cd $folder;
    git pull;
    cd ../;
}else{
    echo "Baixando builds..."
    git clone $gitUrl;
}

echo "Subindo serviço Authorization..."
$functionHmg = "api-authorization-" + $instanceId + "-homolog";
$functionPrd = "api-authorization-" + $instanceId;
$functionBuild = $folder + "/api-authorization.zip";

az functionapp deployment source config-zip -g $resourceGroup -n $functionHmg --src $functionBuild;
# az functionapp deployment source config-zip -g $resourceGroup -n $functionPrd --src $functionBuild;


